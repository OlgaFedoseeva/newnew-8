# подготовка репозитория для последующей публикации
rm -rf .git
git init
git add .git*
git commit -m "инициализация"
git add CONTRIBUTING.md
git add ./*LICENSE*
git commit -m "лицензия"
git add CHANGELOG.md
git add DIRECTORY-TREE.md
git add README*
git commit -m "описание"
git add ./*.sh
git commit -m "скрипты командной строки"
git add ./*.min.css
git add ./*.min.js
git add ./*.woff
git commit -m "сторонние материалы"
git add ./*.bmp
git add ./*.gif
git add ./*.ico
git add ./*.jpg
git add ./*.png
git add ./*.svg
git commit -m "изображения"
find . -type f -not -path './.*' | sort -r | while read -r file; do
 echo "Обработка: $file"
 git add "$file"
 git commit -m "${file#*/}"
done
